-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: polisenger
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKeyword` int(11) DEFAULT NULL,
  `comment` varchar(1000) NOT NULL,
  `submittedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(240) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idKeyword` (`idKeyword`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`idKeyword`) REFERENCES `keywords` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,2,'bad teacher today','2016-12-14 01:23:05','Bogdan'),(2,2,'Udriste is boring i don\' know','2016-12-14 01:24:29','Robert'),(3,2,'bad teacher i don\'t know','2016-12-14 01:37:46','Bogdan'),(4,1,'The light is very bad during the night','2016-12-14 01:38:44','Raluca'),(5,1,'The light is very bad during the night','2016-12-14 01:38:49','Rares'),(6,1,'The light is very bad during the lectures','2016-12-14 01:39:00','Raluca'),(7,2,'bad teacher i don\'t think he knows how hard is to do all those HWs','2016-12-14 01:41:49','Stefan'),(8,2,'bad teacher udriste haha','2016-12-14 16:27:56','Bogdan'),(9,2,'bad teacher today in class','2016-12-15 12:43:36','Bogdan'),(10,2,'bad teacher, hard HWs','2016-12-16 10:22:15','Bogdan'),(11,2,'bad teacher today','2016-12-16 12:34:48','Raluca'),(12,2,'Bad teacher are pula mare','2016-12-16 14:01:27','Qq');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keywordName` varchar(240) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keywords`
--

LOCK TABLES `keywords` WRITE;
/*!40000 ALTER TABLE `keywords` DISABLE KEYS */;
INSERT INTO `keywords` VALUES (1,'Lack of light'),(2,'Bad Teacher'),(3,'Exam'),(4,'Missing class');
/*!40000 ALTER TABLE `keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurements`
--

DROP TABLE IF EXISTS `measurements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measurements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `degree` double DEFAULT NULL,
  `humidity` double DEFAULT NULL,
  `submittedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurements`
--

LOCK TABLES `measurements` WRITE;
/*!40000 ALTER TABLE `measurements` DISABLE KEYS */;
/*!40000 ALTER TABLE `measurements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `name` varchar(240) NOT NULL,
  `username` varchar(240) DEFAULT NULL,
  `submittedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` VALUES (1,44.45694580,26.10349330,'Ganesha','John','2016-12-09 01:34:30'),(5,37.42199833,-122.08400000,'Fils','Bogdan','2016-12-09 01:48:06'),(6,37.42199833,-122.08400000,'Fils','Bogdan','2016-12-09 02:57:31'),(9,37.42199833,-122.08400000,'Poli','Bogdan2','2016-12-09 04:18:28'),(11,37.42199833,-122.08400000,'Politehnica','Bogdan','2016-12-13 03:33:28'),(12,37.42199833,-122.08400000,'Politehnica','Rares','2016-12-13 03:46:54'),(13,37.42199833,-122.08400000,'Poli','Bogdan','2016-12-13 10:29:42'),(14,37.42199833,-122.08400000,'Politehnica','BOgdan','2016-12-13 11:39:30'),(15,37.42199833,-122.08400000,'Politehnica','Mihai','2016-12-13 23:46:24'),(16,37.42199833,-122.08400000,'Politehnica','Bogdan','2016-12-14 16:27:14'),(17,37.42199833,-122.08400000,'Poli','Bogdan','2016-12-16 10:26:58');
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-08 15:41:36
