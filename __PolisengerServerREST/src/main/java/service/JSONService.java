package service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.CommentStatistic;
import model.Location;
import model.Measurement;
import model.TopPlace;

public class JSONService {
	
	public static String measurement2JSON(Measurement measurement){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(measurement);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
	
	public static String measurementsToJSON(List<Measurement> measurements){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int index = 0;
		for(Measurement measurement : measurements){
			if(index==measurements.size()-1){
				sb.append(measurement2JSON(measurement));
			}else{
				sb.append(measurement2JSON(measurement)).append(",");
			}
			index++;
		}
		sb.append("]");
		return sb.toString();
	}
	
	private static String comment2JSON(CommentStatistic comment){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(comment);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
	
	public static String commentsToJSON(List<CommentStatistic> comments){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int index = 0;
		for(CommentStatistic comment : comments){
			if(index==comments.size()-1){
				sb.append(comment2JSON(comment));
			}else{
				sb.append(comment2JSON(comment)).append(",");
			}
			index++;
		}
		sb.append("]");
		return sb.toString();
	}
	private static String place2JSON(Location location){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(location);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
	
	public static String placesToJSON(List<Location> locations){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int index = 0;
		for(Location location : locations){
			if(index==locations.size()-1){
				sb.append(place2JSON(location));
			}else{
				sb.append(place2JSON(location)).append(",");
			}
			index++;
		}
		sb.append("]");
		return sb.toString();
	}
	
	private static String topPlace2JSON(TopPlace place){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(place);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
	
	public static String topPlacesToJSON(List<TopPlace> places){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int index = 0;
		for(TopPlace place : places){
			if(index==places.size()-1){
				sb.append(topPlace2JSON(place));
			}else{
				sb.append(topPlace2JSON(place)).append(",");
			}
			index++;
		}
		sb.append("]");
		return sb.toString();
	}
	
}
