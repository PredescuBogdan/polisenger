package model;

public class TopPlace {
	private String name;
	private int checkIns;
	
	
	public TopPlace() {
		super();
	}

	public TopPlace(String name, int checkIns) {
		super();
		this.name = name;
		this.checkIns = checkIns;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getcheckIns() {
		return checkIns;
	}

	public void setCheckIns(int checkIns) {
		this.checkIns = checkIns;
	}
	
	
}
