package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="comments")
public class Comment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="comment")
	private String comment;
	
	@Column(name="submittedDate")
	private String submittedDate;

	@Column(name="username")
	private String username;
	
	@ManyToOne()
	@JoinColumn(name="idKeyword",insertable=false,updatable=false)
	private Keyword keyword;
	
	public Comment() {
		
	}

	public Comment(Keyword keyword, String comment, String username, String date) {
		this.keyword=keyword;
		this.comment=comment;
		this.submittedDate=date;
		this.username=username;
	}
	
	public Comment(String comment, String submittedDate, String username) {
		super();
		this.comment = comment;
		this.submittedDate = submittedDate;
		this.username = username;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", comment=" + comment + ", submittedDate=" + submittedDate + ", username="
				+ username + ", keyword=" + keyword + "]";
	}
	
}
