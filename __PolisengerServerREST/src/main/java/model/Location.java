package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="places")
public class Location {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="latitude")
	private double latitude;

	@Column(name="longitude")
	private double longitude;
	
	@Column(name="name")
	private String name;

	@Column(name="username")
	private String username;
	
	@Column(name="submittedDate")
	private String submittedDate;
	
	public Location() {
		super();
	}

	public Location(double latitude, double longitude, String name) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
	}

	
	public Location(double latitude, double longitude, String name, String username) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
		this.username = username;
	}

	
	public Location(double latitude, double longitude, String name, String username, String submittedDate) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
		this.username = username;
		this.submittedDate = submittedDate;
	}

	@JsonIgnore
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(String submittedDate) {
		this.submittedDate = submittedDate;
	}
	
}
