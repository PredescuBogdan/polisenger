package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Measurements")
public class Measurement {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="submittedDate")
	private String dateMeasured;
	
	@Column(name="degree")
	private double degree;
	
	@Column(name="humidity")
	private double humidity;
	
	@Transient
	private String location;
	
	public Measurement() {
		super();
	}
	
	
	public Measurement(String dateMeasured, double degreeC, double humidity) {
		super();
		this.dateMeasured = dateMeasured;
		this.degree = degreeC;
		this.humidity = humidity;
	}

	public double getCelsius() {
		return degree;
	}

	public double getFahrenheit() {
		return degree * 9 / 5 + 32;
	}

	public String getDateMeasured() {
		return dateMeasured;
	}

	public void setDateMeasured(String dateMeasured) {
		this.dateMeasured = dateMeasured;
	}
	
	@JsonIgnore
	public double getDegrees() {
		return degree;
	}

	public void setDegrees(double degreeC) {
		this.degree = degreeC;
	}


	public double getHumidity() {
		return humidity;
	}


	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
	
	
	
}
