package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="keywords")
public class Keyword {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer Id;
	
	@Column(name="keywordName")
	private String keywordName;
	
	@OneToMany(mappedBy = "comment",fetch=FetchType.EAGER, cascade = { CascadeType.ALL })
	List<Comment>comments;
	
	public Keyword() {
		
	}

	public Keyword(String keywordName) {
		super();
		this.keywordName = keywordName;
	}

	public String getKeywordName() {
		return keywordName;
	}

	public void setKeywordName(String keywordName) {
		this.keywordName = keywordName;
	}

	@Override
	public String toString() {
		return "Keyword [Id=" + Id + ", keywordName=" + keywordName + ", comments=" + comments + "]";
	}
	
}
