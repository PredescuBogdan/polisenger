package model;

public class CommentStatistic {
	private String submitedDate;
	private String username;
	private String keywordName;
	private String comment;
	
	public CommentStatistic() {
	}
	
	public CommentStatistic(String submitedDate, String username, String keywordName, String comment) {
		super();
		this.submitedDate = submitedDate;
		this.username = username;
		this.keywordName = keywordName;
		this.comment = comment;
	}

	public String getKeywordName() {
		return keywordName;
	}

	public void setKeywordName(String keywordName) {
		this.keywordName = keywordName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getSubmitedDate() {
		return submitedDate;
	}

	public void setSubmitedDate(String submitedDate) {
		this.submitedDate = submitedDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	@Override
	public String toString() {
		return "CommentStatistic [submitedDate=" + submitedDate + ", username=" + username + ", keywordName="
				+ keywordName + ", comment=" + comment + "]";
	}
}
