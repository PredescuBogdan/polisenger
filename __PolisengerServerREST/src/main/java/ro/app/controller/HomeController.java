package ro.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/navigare")
public class HomeController {


	@RequestMapping("/acasa")
	public ModelAndView daOPagina(){
		ModelAndView mav = new ModelAndView("home");
		return mav;
	}
	
	@RequestMapping("/contact")
	public ModelAndView paginaContact(){
		ModelAndView mav = new ModelAndView("contact");
		return mav;
	}
}
