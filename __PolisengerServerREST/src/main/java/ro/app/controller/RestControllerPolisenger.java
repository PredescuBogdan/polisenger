package ro.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import model.CommentStatistic;
import model.Location;
import model.Measurement;
import model.TopPlace;
import ro.app.dao.DAOLocation;
import ro.app.dao.DAOMeasurement;
import service.JSONService;

@RestController
public class RestControllerPolisenger {
	private DAOMeasurement daoTemp = new DAOMeasurement();
	private DAOLocation daoLoc = new DAOLocation();
	private int counter = 0;
	private boolean firstTime = true;
	
	
	//NOT USED ANYMORE - EXTERNAL DB ADDED FOR MEASUREMENTS
	@RequestMapping(value= "/addMeasurement", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET) 
	public void addTemperature(@RequestParam("celsius") String temperature, @RequestParam("humidity") String humidity) {
		if(firstTime) {
			daoTemp.cleanTable();
			firstTime=false;
		}
		counter ++;
		System.err.println("COUNTER: " + counter);
		System.err.println("AM PRIMIT:  " + temperature);
		Date dNow = new Date( );
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	    daoTemp.create(new Measurement(ft.format(dNow),Double.parseDouble(temperature),Double.parseDouble(humidity)));
	    System.err.println("AM INSERAT IN TABEL");
	    if(counter >= 100) {
	    	System.err.println("AM CURATAT TABEL");
	    	daoTemp.cleanTable();
	    	counter = 0;
	    }
	}
	
	//NOT USED ANYMORE - EXTERNAL DB ADDED FOR MEASUREMENTS
	@RequestMapping(value = "/readMeasurementALL", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ModelAndView returnTempAll() {
//		List<Measurement>measurements = daoTemp.findAll();
//		String json = JSONService.measurementsToJSON(measurements);
//		return json;
		return new ModelAndView ("redirect:https://endpointrp-spiredev.c9users.io/jsondata.php"); 
	}

	
	@RequestMapping(value= "/readReviews", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET) 
	public String returnReviews() {
		List<CommentStatistic>comments = ro.app.dao.DAOCommentStatistic.findAll();
		String json = JSONService.commentsToJSON(comments);
		return json;
	}
	
	@RequestMapping(value= "/readLocations", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET) 
	public String returnLocations() {
		List<Location>locations = daoLoc.findAll();
		String json = JSONService.placesToJSON(locations);
		return json;
	}
	
	@RequestMapping(value= "/readTopPlaces", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET) 
	public String returnTopPlaces() {
		List<TopPlace>locations = DAOLocation.findTopPlaces();
		String json = JSONService.topPlacesToJSON(locations);
		return json;
	}
	
	@RequestMapping(value= "/addLocation", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST) 
	public void addLocations(@RequestParam("latitude") double latitude, @RequestParam("longitude")double longitude, 
								@RequestParam("name") String name, @RequestParam("username") String username) {
		Date dNow = new Date( );
	    SimpleDateFormat ft = 
	    new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		daoLoc.create(new Location(latitude, longitude, name,username,ft.format(dNow)));
	}
	
}
