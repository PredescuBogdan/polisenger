package ro.app.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import model.Location;
import model.TopPlace;

public class DAOLocation extends GenericDaoHibernateImpl<Location, Integer>{

	public DAOLocation() {
		super(Location.class);
	}
	
	
	public static List<TopPlace> findTopPlaces(){
		List<TopPlace> topPlaces = new ArrayList<TopPlace>();
		Statement stmt;
		try{
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();
			String sql = "select name, count(*) checkIns from places group by name order by checkIns desc limit 3;";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				String locationName = rs.getString("name");
				int checkInsNo = rs.getInt("checkIns");
				
				topPlaces.add(new TopPlace(locationName,checkInsNo));
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return topPlaces;
	}
}
