package ro.app.dao;

import model.Keyword;

public class DAOKeyword extends GenericDaoHibernateImpl<Keyword, Integer>{

	public DAOKeyword() {
		super(Keyword.class);
	}

}
