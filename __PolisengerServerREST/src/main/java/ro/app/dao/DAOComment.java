package ro.app.dao;

import model.Comment;

public class DAOComment extends GenericDaoHibernateImpl<Comment, Integer>{

	public DAOComment() {
		super(Comment.class);
	}
}
