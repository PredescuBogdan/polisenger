package ro.app.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import model.CommentStatistic;

public class DAOCommentStatistic {
	
	public static List<CommentStatistic> findAll(){
		List<CommentStatistic> opinions = new ArrayList<CommentStatistic>();
		Statement stmt;
		try{
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();
			
			String sql = "select keywordName, comment, submittedDate,username from comments "
						+ "left join keywords ON comments.idKeyword = keywords.id ORDER BY submittedDate desc;";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				CommentStatistic cs = new CommentStatistic(rs.getString("submittedDate"), rs.getString("username"), 
									  rs.getString("keywordName"),rs.getString("comment"));
				opinions.add(cs);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return opinions;
	}
}
