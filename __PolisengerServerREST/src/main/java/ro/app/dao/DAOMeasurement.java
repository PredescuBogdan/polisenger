package ro.app.dao;

import org.hibernate.Query;
import org.hibernate.Session;

import model.Measurement;



public class DAOMeasurement extends GenericDaoHibernateImpl<Measurement, Integer>{
	
	public DAOMeasurement() {
		super(Measurement.class);
	}
	
	public String returnLastTemp() {
		Session session = getSession();
		Query query = session.createQuery("from Measurement m order by m.dateMeasured desc");
		query.setMaxResults(1);
		String lastTemp = query.getQueryString();
		return lastTemp;
	}
	
	public void cleanTable(){
		Session session = getSession();
		Query query = session.createQuery("delete Measurement");
		query.executeUpdate();
	}
}
