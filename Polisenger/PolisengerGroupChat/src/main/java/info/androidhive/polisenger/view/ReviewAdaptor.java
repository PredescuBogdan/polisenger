package info.androidhive.polisenger.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Filter;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import info.androidhive.polisenger.R;
import info.androidhive.polisenger.model.Review;

/**
 * Created by Bogdan on 12/13/2016.
 */

public class ReviewAdaptor extends BaseAdapter implements Filterable {

    private Context context;
    private List<Review> reviewsList;
    private ValueFilter valueFilter;
    private List<Review>tempList;
    private ArrayList<Review> filterList;


    public ReviewAdaptor(Context context, List<Review> reviewsList ) {
        this.context = context;
        this.reviewsList = reviewsList;
        tempList = reviewsList;
    }

    @Override
    public int getCount() {
        return reviewsList.size();
    }

    @Override
    public Object getItem(int i) {
        return reviewsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Review review = reviewsList.get(i);

        view = inflater.inflate(R.layout.review_item, null);

       TextView tvDate = (TextView) view.findViewById(R.id.reviewDate);
       TextView tvUsername = (TextView) view.findViewById(R.id.reviewUsername);
       TextView tvKeyword = (TextView) view.findViewById(R.id.reviewKeyword);
       TextView tvComment = (TextView) view.findViewById(R.id.reviewComment);

        tvDate.setText("Date: " + review.getDate());
        tvUsername.setText("Username: " + review.getUsername());
        tvKeyword.setText("Keyword: " + review.getKeyword());
        tvComment.setText("Comment: " + review.getComment());

        return view;
    }



    @Override
    public Filter getFilter() {
        if(valueFilter==null) {
            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }
    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            filterList=new ArrayList<Review>();
            if(constraint!=null && constraint.length()>0){
                for(int i=0;i<reviewsList.size();i++){
                    if((reviewsList.get(i).getKeyword().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        String date = reviewsList.get(i).getDate();
                        String username = reviewsList.get(i).getUsername();
                        String comment = reviewsList.get(i).getComment();
                        String keyword = reviewsList.get(i).getKeyword();
                        Review review = new Review(date,username,comment,keyword);
                        filterList.add(review);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else {
                results.count=tempList.size();
                results.values=tempList;
                }
            return results;
        }

        //Invoked in the UI thread to publish the filtering results in the user interface.
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            reviewsList=(List<Review>) results.values;
            notifyDataSetChanged();
        }
    }
}
