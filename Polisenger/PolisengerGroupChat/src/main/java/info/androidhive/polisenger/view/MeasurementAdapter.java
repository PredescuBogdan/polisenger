package info.androidhive.polisenger.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import info.androidhive.polisenger.R;
import info.androidhive.polisenger.model.Measurement;

/**
 * Created by Bogdan on 12/12/2016.
 */

public class MeasurementAdapter extends BaseAdapter{

    private Context context;
    private List<Measurement> measurementList;

    public MeasurementAdapter(Context context, List<Measurement> data){
        this.context = context;
        this.measurementList = data;
    }


    @Override
    public int getCount() {
        return measurementList.size();
    }

    @Override
    public Object getItem(int i) {
        return measurementList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Measurement measurement = measurementList.get(i);

        view = inflater.inflate(R.layout.measurement_item, null);

        TextView tvDate = (TextView)view.findViewById(R.id.textViewDateMeasured);
        TextView tvTemperature = (TextView)view.findViewById(R.id.textViewLastTemperature);
        TextView tvHumidity = (TextView)view.findViewById(R.id.textViewLastHumidity);

        tvDate.setText("Date: " + measurement.getDateMeasured());
        tvTemperature.setText("Temperature: " + measurement.getCelsius() + "°C");
        tvHumidity.setText("Humidity: " + +measurement.getHumidity() + "%");

        return view;
    }
}
