package info.androidhive.polisenger;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ActivityAbout extends Activity {

    private TextView textAbout;
    private TextView textTeam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
        setContentView(R.layout.activity_about);

        textAbout = (TextView) findViewById(R.id.textViewAbout);
        textAbout.setText("This is an Android Chat App using Sockets \n\nDesigned for POLITEHNICA");

        textTeam = (TextView)findViewById(R.id.textViewTeam);
        textTeam.setText("TEAM: \n" +
                  "Predescu Bogdan");
    }
}


