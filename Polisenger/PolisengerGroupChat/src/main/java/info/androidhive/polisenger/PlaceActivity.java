package info.androidhive.polisenger;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

import info.androidhive.polisenger.http.HttpHandler;
import info.androidhive.polisenger.model.Measurement;
import info.androidhive.polisenger.model.Place;
import info.androidhive.polisenger.other.IConstants;
import info.androidhive.polisenger.service.JSONService;
import info.androidhive.polisenger.view.MeasurementAdapter;
import info.androidhive.polisenger.view.PlaceAdaptor;

public class PlaceActivity extends Activity {

    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);
        listView = (ListView)findViewById(R.id.listViewPlacesAll);

        IncarcatorDeJsoane incarcatordDeJsoane = new IncarcatorDeJsoane();
        incarcatordDeJsoane.execute();
    }

    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(IConstants.SERVER_URL+"/readLocations");
            System.out.print(json);
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            System.out.println("DATELE DE LA SERVER: " + s);
            List<Place> places = JSONService.placeToList(s);

            PlaceAdaptor adaptor = new PlaceAdaptor(PlaceActivity.this,places);
            listView.setAdapter(adaptor);
            super.onPostExecute(s);

        }
    }
}
