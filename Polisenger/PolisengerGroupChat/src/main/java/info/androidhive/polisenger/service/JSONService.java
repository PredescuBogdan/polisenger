package info.androidhive.polisenger.service;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import info.androidhive.polisenger.model.Measurement;
import info.androidhive.polisenger.model.Place;
import info.androidhive.polisenger.model.Review;

/**
 * Created by Bogdan on 12/6/2016.
 */

public class JSONService {

    public static List<Place> placeToList(String json){
        List<Place> places = new ArrayList<Place>();
        try {
            JSONArray jsonA = new JSONArray(json);
            if(jsonA.length() >=2)
                for(int i=0; i<jsonA.length(); i++){
                    JSONObject obj = jsonA.getJSONObject(i);
                    String date = obj.getString("submittedDate");
                    String name = obj.getString("name");
                    String username =obj.getString("username");
                    String latitude =obj.getString("latitude");
                    String longitude =obj.getString("longitude");

                    Place place = new Place(latitude,longitude,name,username,date);
                    places.add(place);
                }
        }catch(Exception e){
            e.printStackTrace();
        }
        return places;
    }


    public static List<Measurement> tempToList2(String json){
        List<Measurement> temperatures = new ArrayList<Measurement>();
        try {
            JSONArray jsonA = new JSONArray(json);
            if(jsonA.length() >=2)
                for(int i=0; i<jsonA.length(); i++){
                    JSONObject obj = jsonA.getJSONObject(i);
                    String date = obj.getString("submittedDate");
                    String tempInCelsius = obj.getString("degree");
                    String humidity =obj.getString("humidity");
                    Measurement measurement = new Measurement(date, Double.valueOf(tempInCelsius), Double.valueOf(humidity));
                    temperatures.add(measurement);
                }
        }catch(Exception e){
            e.printStackTrace();
        }
        return temperatures;
    }

    public static List<Review> reviewsToList(String json) {
        List<Review>reviews = new ArrayList<Review>();
        try{
            JSONArray jsonA = new JSONArray(json);
            String s = "----------------------------------------------------------------------";
            for(int i=0; i<jsonA.length(); i++){
                JSONObject obj = jsonA.getJSONObject(i);
                String keyword = obj.getString("keywordName");
                String comment = obj.getString("comment");
                String date = obj.getString("submitedDate");
                String username = obj.getString("username");

                Review review = new Review(date,username,comment,keyword);
                reviews.add(review);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        return reviews;
    }


    public static List<Place> topPlaceToList(String json){
        List<Place> places = new ArrayList<Place>();
        try {
            JSONArray jsonA = new JSONArray(json);
            if(jsonA.length() >=2)
                for(int i=0; i<jsonA.length(); i++){
                    JSONObject obj = jsonA.getJSONObject(i);
                    String name = obj.getString("name");
                    String checkInNo = obj.getString("checkIns");
                    Place place = new Place(name,checkInNo);
                    places.add(place);
                }
        }catch(Exception e){
            e.printStackTrace();
        }
        return places;
    }
}
