package info.androidhive.polisenger;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import info.androidhive.polisenger.http.HttpHandler;
import info.androidhive.polisenger.model.Measurement;
import info.androidhive.polisenger.other.IConstants;
import info.androidhive.polisenger.service.JSONService;
import info.androidhive.polisenger.view.MeasurementAdapter;

public class TemperatureActivity extends Activity {


    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);


        listView = (ListView)findViewById(R.id.listViewTemperaturi);

        System.out.println();
        IncarcatorDeJsoane incarcatorDeJsoane = new IncarcatorDeJsoane();
        incarcatorDeJsoane.execute();
    }

    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String>{


        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(IConstants.MEASUREMENT_EXTERNAL_C9_URL); //("http://192.168.56.1:9080/__PolisengerServerREST/readMeasurementALL");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            System.out.println("DATELE DE LA SERVER: " + s);
            List<Measurement> temperatures = JSONService.tempToList2(s);

            MeasurementAdapter adaptor = new MeasurementAdapter(TemperatureActivity.this, temperatures);
//            ArrayAdapter<Measurement> itemsAdapter =
//                    new ArrayAdapter<Measurement>(TemperatureActivity.this, android.R.layout.simple_list_item_1, temperatures);
//            listView.setAdapter(itemsAdapter);
            listView.setAdapter(adaptor);
            super.onPostExecute(s);

        }
    }

}
