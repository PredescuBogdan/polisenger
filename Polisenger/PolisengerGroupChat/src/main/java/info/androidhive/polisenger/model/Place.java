package info.androidhive.polisenger.model;

/**
 * Created by Bogdan on 12/13/2016.
 */

public class Place {


    private String latitude;
    private String longitude;
    private String name;
    private String username;
    private String submittedDate;
    private String checkIn;


    public Place(String name, String checkIn) {
        this.name = name;
        this.checkIn = checkIn;
    }

    public Place(String latitude, String longitude, String name, String username, String submittedDate) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.username = username;
        this.submittedDate = submittedDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(String submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }
}
