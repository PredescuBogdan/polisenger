package info.androidhive.polisenger;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import info.androidhive.polisenger.http.HttpHandler;
import info.androidhive.polisenger.model.Place;
import info.androidhive.polisenger.other.IConstants;
import info.androidhive.polisenger.service.JSONService;
import info.androidhive.polisenger.view.TopPlaceAdaptor;

public class TopPlaceActivity extends Activity {

    private ListView listView;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_place);

        listView = (ListView)findViewById(R.id.listViewTopPlaces);

        button =(Button)findViewById(R.id.TopPlaceButtonLog);
        final Animation submit = AnimationUtils.loadAnimation(this,R.anim.anim_button);
        submit.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(TopPlaceActivity.this, PlaceActivity.class);
                startActivity(intent);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view.startAnimation(submit);


            }
        });

        IncarcatorDeJsoane IncarcatorDeJsoane = new IncarcatorDeJsoane();
        IncarcatorDeJsoane.execute();

    }


    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(IConstants.SERVER_URL+"/readTopPlaces");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {

            List<Place> places = JSONService.topPlaceToList(s);

            TopPlaceAdaptor adaptor = new TopPlaceAdaptor(places, TopPlaceActivity.this);
            listView.setAdapter(adaptor);
            super.onPostExecute(s);

        }
    }

}

