package info.androidhive.polisenger.model;

/**
 * Created by Bogdan on 12/12/2016.
 */


public class Measurement {

    private Integer id;

    private String dateMeasured;

    private double degree;

    private double humidity;


    public Measurement(String dateMeasured, double degreeC, double humidity) {
        super();
        this.dateMeasured = dateMeasured;
        this.degree = degreeC;
        this.humidity = humidity;
    }

    public double getCelsius() {

        return degree;
    }

    public double getFahrenheit() {

        return degree * 9 / 5 + 32;
    }

    public String getDateMeasured() {

        return dateMeasured;
    }


    public double getHumidity() {
        return humidity;
    }


    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }



}
