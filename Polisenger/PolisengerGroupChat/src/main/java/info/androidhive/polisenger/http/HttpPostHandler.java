package info.androidhive.polisenger.http;

import android.location.Location;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import info.androidhive.polisenger.other.IConstants;

/**
 * Created by Bogdan on 12/8/2016.
 */

public class HttpPostHandler {

    // HTTP POST request
    public void sendPost(Location location, String name, String username) throws Exception {


        String url = IConstants.SERVER_URL+"/addLocation"; // +"?latitude="+location.getLatitude()+"&longitude="+location.getLongitude()+"&name=DEFAULT";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        System.out.println(url);
        String urlParameters = "latitude="+location.getLatitude()+"&longitude="+location.getLongitude()+"&name="+name + "&username="+username;

        System.out.println(urlParameters);
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();


        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

}
