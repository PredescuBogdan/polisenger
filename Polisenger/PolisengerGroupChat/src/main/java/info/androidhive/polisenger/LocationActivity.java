package info.androidhive.polisenger;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import info.androidhive.polisenger.http.HttpPostHandler;

public class LocationActivity extends Activity {

    private static final String TAG = LocationActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);


        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new MyLocationListener();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);



        final Location locatieAcum = getLocation();

        final Button button = (Button)findViewById(R.id.buttonSendLocation);
        final Animation submit = AnimationUtils.loadAnimation(this,R.anim.anim_button);
        submit.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final EditText textView = (EditText) findViewById(R.id.editTextLocationName);
                String locationName = textView.getText().toString();
                LocationSender ls = new LocationSender(locationName);


                if (textView.getText().toString().trim().length() > 0) {
                    ls.execute(locatieAcum);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter a location name", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                view.startAnimation(submit);
            }
        });
        Log.d(TAG, ""+locatieAcum);

    }

    private class LocationSender extends AsyncTask<Location, Void, Void>{

        private String loc;

        public LocationSender(String loc){
            this.loc = loc;
        }

        @Override
        protected Void doInBackground(Location... locations) {
            System.out.println("PAS 1 " + locations[0]);
            final HttpPostHandler hph = new HttpPostHandler();
            try {

                hph.sendPost(locations[0], loc, getIntent().getStringExtra("username"));
                Log.d("DEBUG USER: ", getIntent().getStringExtra("username"));
            }catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }


    }

    public Location getLocation()
    {
        // Get the location manager
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(bestProvider);
        return location;
    }

    private class MyLocationListener implements LocationListener {

        private final String TAG = MyLocationListener.class.getName();

        @Override
        public void onLocationChanged(Location loc) {
            // editLocation.setText("");
            // pb.setVisibility(View.INVISIBLE);
            Toast.makeText(
                    getBaseContext(),
                    "Location changed: Lat: " + loc.getLatitude() + " Lng: "
                            + loc.getLongitude(), Toast.LENGTH_SHORT).show();
            String longitude = "Longitude: " + loc.getLongitude();
            Log.v(TAG, longitude);
            String latitude = "Latitude: " + loc.getLatitude();
            Log.v(TAG, latitude);

        /*------- To get city name from coordinates -------- */
            String cityName = null;
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(loc.getLatitude(),
                        loc.getLongitude(), 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            String s = longitude + "\n" + latitude + "\n\nMy Current City is: "
                    + cityName;
            // editLocation.setText(s);
            Log.d("TEST", s);
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }
}
