package info.androidhive.polisenger.view;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import info.androidhive.polisenger.R;
import info.androidhive.polisenger.model.Place;

/**
 * Created by Bogdan on 12/13/2016.
 */

public class TopPlaceAdaptor extends BaseAdapter{


    private List<Place> topPlacesList;
    private Context context;


    public TopPlaceAdaptor(List<Place> data, Context context) {
        this.topPlacesList = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return topPlacesList.size();
    }

    @Override
    public Object getItem(int i) {
        return topPlacesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Place topPlace = topPlacesList.get(i);

        view = inflater.inflate(R.layout.top_place_item, null);

        TextView tvPlaceName = (TextView)view.findViewById(R.id.topPlaceName);
        TextView tvCheckInNo = (TextView)view.findViewById(R.id.topPlaceCheckInNo);

        tvPlaceName.setText("Place Name: " + topPlace.getName());
        tvCheckInNo.setText("Total Check-Ins: " + topPlace.getCheckIn());

        return view;
    }
}
