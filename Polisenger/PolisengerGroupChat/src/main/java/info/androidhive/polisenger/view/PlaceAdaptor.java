package info.androidhive.polisenger.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import info.androidhive.polisenger.R;
import info.androidhive.polisenger.model.Place;

/**
 * Created by Bogdan on 12/13/2016.
 */

public class PlaceAdaptor extends BaseAdapter {

    private Context context;
    private List<Place> placetList;

    public PlaceAdaptor(Context context, List<Place> data){
        this.context = context;
        this.placetList = data;
    }


    @Override
    public int getCount() {
        return placetList.size();
    }

    @Override
    public Object getItem(int i) {
        return placetList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Place place = placetList.get(i);

        view = inflater.inflate(R.layout.place_item, null);

        TextView tvDate = (TextView)view.findViewById(R.id.textViewPlaceDate);
        TextView tvUsername = (TextView)view.findViewById(R.id.textViewPlaceUsername);
        TextView tvPlaceName = (TextView)view.findViewById(R.id.textViewPlaceName);
        TextView tvPlaceLatitude = (TextView)view.findViewById(R.id.textViewPlaceLatitude);
        TextView tvPlaceLongitude = (TextView)view.findViewById(R.id.textViewPlaceLongitude);

        tvDate.setText("Date: " + place.getSubmittedDate());
        tvUsername.setText("Username: " + place.getUsername());
        tvPlaceName.setText("Place Name: " + place.getName());
        tvPlaceLatitude.setText("Latitude: " + place.getLatitude());
        tvPlaceLongitude.setText("Longitude: " + place.getLongitude());

        return view;
    }
}
