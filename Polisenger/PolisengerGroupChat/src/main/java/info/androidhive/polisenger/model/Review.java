package info.androidhive.polisenger.model;

/**
 * Created by Bogdan on 12/13/2016.
 */

public class Review {

    private String date;
    private String username;
    private String comment;
    private String keyword;

    public Review(String keyword) {
        this.keyword = keyword;
    }

    public Review(String date, String username, String comment, String keyword) {
        this.date = date;
        this.username = username;
        this.comment = comment;
        this.keyword = keyword;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
