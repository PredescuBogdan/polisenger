package info.androidhive.polisenger;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ListView;

import java.util.List;

import info.androidhive.polisenger.http.HttpHandler;
import info.androidhive.polisenger.model.Review;
import info.androidhive.polisenger.other.IConstants;
import info.androidhive.polisenger.service.JSONService;
import info.androidhive.polisenger.view.ReviewAdaptor;

public class ReviewsActivity extends Activity {

    private ListView listView;
    private EditText searchMessage;
    private ReviewAdaptor ra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        listView = (ListView)findViewById(R.id.listReviews);
        searchMessage = (EditText)findViewById(R.id.inputSearch);


        ReviewsActivity.IncarcatorDeJsoane incarcatorDeJsoane = new ReviewsActivity.IncarcatorDeJsoane();
        incarcatorDeJsoane.execute();
    }

    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(IConstants.SERVER_URL+"/readReviews");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            System.out.println("DATELE DE LA SERVER: " + s);
            final List<Review> reviews = JSONService.reviewsToList(s);
            ra = new ReviewAdaptor(ReviewsActivity.this, reviews);
            searchMessage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    ra.getFilter().filter(charSequence);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

//            ArrayAdapter<String> itemsAdapter =
//                    new ArrayAdapter<String>(ReviewsActivity.this, android.R.layout.simple_list_item_1, reviews);
            listView.setAdapter(ra);
            super.onPostExecute(s);
        }
    }

}
