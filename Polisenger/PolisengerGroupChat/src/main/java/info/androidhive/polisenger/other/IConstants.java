package info.androidhive.polisenger.other;

/**
 * Created by Bogdan on 12/8/2016.
 */

public interface IConstants {
    public String SERVER_URL = "http://172.20.10.2:9080/__PolisengerServerREST";
    public String MEASUREMENT_EXTERNAL_C9_URL = "https://endpointrp-spiredev.c9users.io/jsondata.php";
}
