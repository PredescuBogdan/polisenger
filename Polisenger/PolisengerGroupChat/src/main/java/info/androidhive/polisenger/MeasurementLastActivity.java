package info.androidhive.polisenger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import info.androidhive.polisenger.http.HttpHandler;
import info.androidhive.polisenger.model.Measurement;
import info.androidhive.polisenger.other.IConstants;
import info.androidhive.polisenger.service.JSONService;

public class MeasurementLastActivity extends Activity {

    private TextView textViewTemperature;
    private TextView textViewHumidity;
    private TextView textViewDate;
    private ImageView imageTemp;
    private ImageView imageHum;
    private Handler mHandler = new Handler();
    private Runnable mRefresher = new Runnable() {
        @Override
        public void run() {
            IncarcatorDeJsoane incarcatorDeJsoane = new IncarcatorDeJsoane();
            incarcatorDeJsoane.execute();
            mHandler.postDelayed(this, 3000);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement_last);

        textViewTemperature = (TextView) findViewById(R.id.textViewLastTemperature);
        textViewHumidity = (TextView) findViewById(R.id.textViewLastHumidity);
        textViewDate = (TextView) findViewById(R.id.textViewLastMeasurementDate);
        Button buttonLogMeasurements = (Button) findViewById(R.id.buttonLogMeasurement);
        imageTemp = (ImageView) findViewById(R.id.imageTemperature);
        imageTemp.setVisibility(View.GONE);
        imageHum = (ImageView) findViewById(R.id.imageHumidity);
        imageHum.setVisibility(View.GONE);
        mHandler.postDelayed(mRefresher, 3000);
        final Animation submit = AnimationUtils.loadAnimation(this, R.anim.anim_button);

        submit.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(MeasurementLastActivity.this, TemperatureActivity.class);
                startActivity(intent);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        buttonLogMeasurements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(submit);

            }
        });


    }

    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.lastMeasurementProgressBar);

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(IConstants.MEASUREMENT_EXTERNAL_C9_URL);//"http://192.168.56.1:9080/__PolisengerServerREST/readMeasurementALL");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            List<Measurement> temperatures = JSONService.tempToList2(s);
            Measurement last = temperatures.get(0);
            textViewDate.setText(last.getDateMeasured());
            textViewTemperature.setText(String.valueOf(last.getCelsius()) + "°C");
            textViewHumidity.setText(String.valueOf(last.getHumidity()) + " %");
            imageTemp.setVisibility(View.VISIBLE);
            imageHum.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            super.onPostExecute(s);

        }
    }
}
