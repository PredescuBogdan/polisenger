package com.bogdan.async.http.socketio;

public interface ReconnectCallback {
    public void onReconnect();
}