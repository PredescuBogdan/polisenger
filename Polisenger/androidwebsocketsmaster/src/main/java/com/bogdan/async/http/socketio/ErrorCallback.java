package com.bogdan.async.http.socketio;

public interface ErrorCallback {
    void onError(String error);
}
