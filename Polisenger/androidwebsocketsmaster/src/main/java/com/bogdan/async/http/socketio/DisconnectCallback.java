package com.bogdan.async.http.socketio;

public interface DisconnectCallback {
    void onDisconnect(Exception e);
}
