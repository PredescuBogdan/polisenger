package util;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import dao.DAOComment;

public class KeywordsUtil {
	private static DAOComment daoCom = new DAOComment();
	
	public static String getElemnentFromJson(String JSON, String lookingField) {
		String message = JSON;
		JSONObject jObj;
		try {
			jObj = new JSONObject(message);
			message = jObj.getString(lookingField);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return message;
	}
	
	public static void checker(String JSON, ArrayList<String>keywords, String username){
		int index = 0;
		String message = getElemnentFromJson(JSON, "message");
		for(String s: keywords) {
			index++;
			if(message.toLowerCase().contains(s.toLowerCase())){
				message = message.replace("'", File.separator + "'");
				daoCom.insertComment(index,message, username);
				System.out.println("*****************");
				System.out.println("Mesaj cheie gasit de la " + username + ": " + "'" + message + "'");
				System.out.println("*****************");
			}
		}
	}
}
