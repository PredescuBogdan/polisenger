package model;

public class Comment {
	private int idKeyword;
	private String content;
	private String userFrom;
	
	public Comment(int idKeyword, String content, String userFrom) {
		this.idKeyword = idKeyword;
		this.content = content;
		this.userFrom = userFrom;
	}

	public int getIdKeyword() {
		return idKeyword;
	}

	public void setIdKeyword(int idKeyword) {
		this.idKeyword = idKeyword;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserFrom() {
		return userFrom;
	}

	public void setUserFrom(String userFrom) {
		this.userFrom = userFrom;
	}
	
	
}
