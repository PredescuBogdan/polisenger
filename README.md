#README#
RESTful based web application. Used web sockets in order to facilitate communication between an Android device, a Raspberry PI device and a “central” web application. 
Data was centralised in a relation database (MySQL) and exposed (when necessary via REST services). 
The project made use of websockets and REST web services. 
There was also a JavaScript web component.